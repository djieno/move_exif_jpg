#!/bin/bash

function needs_dirs () {
# creating needed dirs for copying all relevant files
    for p in exif json no_exif

    do
        if [ -d $i/$p ]; then
            echo "$i has $p" > /dev/null

        else
            # echo "$i needs $p"
            mkdir $i/$p

        fi 
    done

}

function create_dirs {
# make sure all files have needs_dirs folder to copy relevant files.
    DIRS=$(ls -d */ | egrep -v 'exif|json|no_exif|testfiles')

    for i in $DIRS

    do 

        needs_dirs   

    done

}

function expand_dir () {
# expanding path or file

    path=$(dirname $i)
    file=$(basename="${i##*/}")
}

function verify_exif () {
# validation exif information from jpg

    EXIF=$(jhead -ft $i |grep "contains")

}

function move_json {
# moving unwanted json to keep overview

    for i in $(ls -d -- **/*.json)

    do 

        expand_dir
        mv $i $path/json/$file

    done
}

function move_exif_jpg {
# moving exif and no_exif files to keep overview

    for i in $(ls -d -- **/*.jpg)

    do 

        expand_dir
        verify_exif

        if [ -z "$EXIF" ]; then
        
            # echo "file has exif"
            mv $i $path/exif/$file

        else
            # echo "file has no information"
            mv $i $path/no_exif/$file
        fi

    done
}

function copy_exif_jpg () {
# copy all exif jpg to one folder for uploading to photos.google.com

    mkdir all_exif_jpg/
    for i in $(ls -d -- */exif/*.jpg)

    do 

        
        cp $i all_exif_jpg/

    done

}

function remove_spaces_folder_files () {

    find -name "* *" -print0 | sort -rz | while read -d $'\0' f; do mv -v "$f" "$(dirname "$f")/$(basename "${f// /_}")"; done
    
}

copy_exif_jpg
create_dirs
move_json
move_exif_jpg
