#!/bin/bash

for i in 1 2 3
    do
        rm -fr dir$i
        mkdir dir$i
        cp testfiles/file_exif dir$i/file_exif$i.jpg
        cp testfiles/file_no_exif dir$i/file_no_exif$i.jpg
        touch dir$i/json$i.json
    done